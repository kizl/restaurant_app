json.array!(@dishes) do |dish|
  json.extract! dish, :id, :title, :description, :price, :quantity
  json.url dish_url(dish, format: :json)
end
