class CreateDishes < ActiveRecord::Migration
  def change
    create_table :dishes do |t|
      t.string :title
      t.text :description
      t.float :price
      t.integer :quantity

      t.timestamps
    end
  end
end
